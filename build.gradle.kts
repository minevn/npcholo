import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("java")
    kotlin("jvm") version "1.7.10"
}

group = "net.minevn"
version = "1.0-SNAPSHOT"

repositories {
    mavenLocal()
    mavenCentral()
    maven("https://repo.codemc.io/repository/nms/")
    maven("https://jitpack.io")
    maven("https://maven.citizensnpcs.co/repo")
    maven("https://repo.extendedclip.com/content/repositories/placeholderapi/")
}

dependencies {
    compileOnly(kotlin("stdlib-jdk8"))
    compileOnly("org.spigotmc:spigot:1.12.2-R0.1-SNAPSHOT")
    compileOnly("com.github.decentsoftware-eu:decentholograms:2.7.2")
    compileOnly("net.citizensnpcs:citizens-main:2.0.30-SNAPSHOT")
    compileOnly("me.clip:placeholderapi:2.11.2")
}

tasks {
    val jarName = "NPCHolo"
    var originalName = ""
    val path = project.properties["shadowPath"]

    jar {
        originalName = archiveFileName.get()
    }

    register("customCopy") {
        dependsOn(jar)

        doLast {
            if (path != null) {
                println("Copying $originalName to $path")
                val to = File("$path/$originalName")
                val rename = File("$path/$jarName.jar")
                File(project.projectDir, "build/libs/$originalName").copyTo(to, true)
                if (rename.exists()) rename.delete()
                to.renameTo(rename)
                println("Copied")
            }
        }
    }


    assemble {
        dependsOn(get("customCopy"))
    }
}


val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "17"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "17"
}