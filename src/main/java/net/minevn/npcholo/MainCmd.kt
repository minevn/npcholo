package net.minevn.npcholo

import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

class MainCmd : CommandExecutor {
	override fun onCommand(sender: CommandSender, cmd: Command?, label: String?, args: Array<out String>?): Boolean {
		if (args.isNullOrEmpty()) {
			sendInfo(sender)
			return true
		}
		when (args[0]) {
			"reload" -> NPCHologram.load()
		}
		return true
	}

	private fun sendInfo(sender: CommandSender) {
		sender.apply {
			sendMessage("§e/npch reload")
		}
	}
}