package net.minevn.npcholo

import eu.decentsoftware.holograms.api.DHAPI
import eu.decentsoftware.holograms.api.holograms.Hologram
import me.clip.placeholderapi.PlaceholderAPI
import net.citizensnpcs.api.CitizensAPI
import net.citizensnpcs.api.npc.NPC
import net.citizensnpcs.trait.SkinTrait
import org.bukkit.Bukkit
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.LivingEntity
import org.bukkit.scheduler.BukkitRunnable
import java.io.File
import java.util.logging.Level

class NPCHologram(private val id: String, private val lines: List<String>, private val npcID: Int,
				  private val skinName: String?) : BukkitRunnable() {
	private var npc: NPC? = null
	private var hologram: Hologram? = null
	private var tick: Int = 0

	init {
		runTaskTimerAsynchronously(NPCHolo.instance, 1, 20)
	}

	private fun updateNPC() {
		if (npc == null) {
			npc = CitizensAPI.getNPCRegistry().getById(npcID)
		}
		if (hologram == null) {
			hologram = npc?.let {
				val location = (it.entity as? LivingEntity)?.eyeLocation ?: return@let null
				DHAPI.createHologram("npcholo_$id", location, lines)
			}
		}
		if (hologram != null && npc != null) {
			(npc!!.entity as? LivingEntity)?.eyeLocation?.let {
				DHAPI.moveHologram(hologram, it.add(0.0, hologram!!.getPage(0).height + 0.4, 0.0))
			}
		}
	}

	private fun updateNPCSkin() {
		if (npc != null && skinName != null) {
			val anyPlayer = Bukkit.getOnlinePlayers().toList().takeIf { it.isNotEmpty() }?.get(0) ?: return
			val name = PlaceholderAPI.setPlaceholders(anyPlayer, skinName).takeIf { it.isNotEmpty() } ?: return
			val skin = MySQL.instance.getSkin(name) ?: return
			runSync {
				val trait = npc!!.getOrAddTrait(SkinTrait::class.java)
				if (trait.signature != skin.signature) {
					val loc = npc!!.storedLocation
					npc!!.despawn()
					trait.setTexture(skin.value, skin.signature)
					npc!!.spawn(loc)
				}
			}
		}
	}

	override fun run() {
		updateNPC()
		if (tick % 60 == 0) updateNPCSkin()
		tick++
		if (tick > 600) tick = 0
	}

	private fun destroy() {
		cancel()
		hologram?.delete()
	}

	companion object {
		private val list = mutableMapOf<String, NPCHologram>()

		fun load() {
			val main = NPCHolo.instance
			main.logger.info("Loading holograms...")
			run {
				clear()
				val folder = main.dataFolder
				val file = File(folder, "holograms.yml").takeIf { it.exists() && !it.isDirectory } ?: return@run
				val config = YamlConfiguration.loadConfiguration(file) ?: return@run
				config.getKeys(false)!!.forEach { id ->
					try {
						val section = config.getConfigurationSection(id)!!
						val lines = section.getStringList("lines")//.colorCodes() // DHAPI auto parse color?
						val npcID = section.getInt("npc.id")
						val skin = section.getString("npc.skin")
						list[id] = NPCHologram(id, lines, npcID, skin)
					} catch (ex: Exception) {
						NPCHolo.instance.logger.log(Level.WARNING, "Can't load NPC holo config id $id", ex)
					}
				}
			}
			main.logger.info("Loaded ${list.size} holograms")
		}
		fun clear() {
			list.values.forEach { it.destroy() }
			list.clear()
		}
	}
}