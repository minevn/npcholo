package net.minevn.npcholo

import java.sql.*
import java.util.logging.Level

class MySQL(private val main: NPCHolo, host: String, dbname: String, user: String, password: String, port: Int) {
	private val sql = run {
		try {
			main.logger.info("Dang ket noi den MySQL")
			try {
				Class.forName("com.mysql.cj.jdbc.Driver")
			} catch (ex1: Exception) {
				Class.forName("com.mysql.jdbc.Driver")
			}
			DriverManager.getConnection("jdbc:mysql://$host:$port/$dbname", user, password)
		} catch (ex: Exception) {
			main.logger.info("Khong the ket noi MySQL")
			main.server.shutdown()
			null
		}
	}

	init {
		instance = this
	}

	fun getSkin(name: String) : SkinValue? {
		var result: ResultSet? = null
		var stm: PreparedStatement? = null
		return try {
			stm = sql!!.prepareStatement(skinSelect)
			stm.setString(1, name)
			result = stm.executeQuery()
			if (result.next()) {
				SkinValue(result.getString(1), result.getString(2))
			}
			else if (name != defaultSkin) getSkin(defaultSkin) else null
		} catch (ex: SQLException) {
			main.logger.log(Level.SEVERE, "Can't get skin data of $name", ex)
			null
		} finally {
			cleanup(result, stm)
		}
	}

	private fun cleanup(result: ResultSet?, statement: Statement?) {
		try {
			result?.close()
		} catch (e: SQLException) {
			main.logger.log(Level.SEVERE, "SQLException on cleanup", e)
		}
		try {
			statement?.close()
		} catch (e: SQLException) {
			main.logger.log(Level.SEVERE, "SQLException on cleanup", e)
		}
	}

	companion object {
		// region sql
		private const val skinSelect = "SELECT `Value`, `Signature` from skin WHERE `Nick` = ?"
		// endregion

		private const val defaultSkin = "skinslib_macdinh_nam"

		lateinit var instance: MySQL
			private set
	}
}