package net.minevn.npcholo

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask

class NPCHolo : JavaPlugin() {
	override fun onEnable() {
		instance = this
		saveDefaultConfig()
		val config = config
		config.getConfigurationSection("mysql")?.let { s ->
			val host = s.getString("host")
			val dbname = s.getString("dbname")
			val user = s.getString("user")
			val password = s.getString("password")
			val port = s.getInt("port")
			MySQL(this, host, dbname, user, password, port)
		}
		getCommand("npcholo")!!.executor = MainCmd()
		runForceSync { NPCHologram.load() }
	}

	override fun onDisable() {
		NPCHologram.clear()
	}

	companion object {
		@JvmStatic
		lateinit var instance: NPCHolo
			private set
	}
}

/**
 * Chuyển mã màu & → §
 */
fun String.colorCodes() = ChatColor.translateAlternateColorCodes('&', this)!!

/**
 * Chuyển mã màu & → §
 */
fun List<String>.colorCodes() = this.map { it.colorCodes() }

fun runAsync(r: Runnable) {
	if (Bukkit.isPrimaryThread()) Bukkit.getScheduler().runTaskAsynchronously(NPCHolo.instance, r)
	else r.run()
}

fun runSync(r: Runnable) {
	if (!Bukkit.isPrimaryThread()) Bukkit.getScheduler().runTask(NPCHolo.instance, r)
	else r.run()
}

fun runForceSync(r: Runnable) {
	Bukkit.getScheduler().runTask(NPCHolo.instance, r)
}

fun scheduleAsync(r: Runnable, period: Long): BukkitTask {
	return Bukkit.getScheduler().runTaskTimerAsynchronously(NPCHolo.instance, r, 1L, period)
}